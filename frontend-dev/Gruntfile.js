﻿function GruntConfig() {
    "use strict";

    /**
     * load config
     */
    var config,
        fs = require('fs'),
        _ = require('lodash'),
        glob = require('glob'),
        cwd = process.cwd(),
        distDirectory,
        globalConfigurations = {};

    (function init() {

        /**
         * set absolute paths for sources and configuration files
         * in case we have to change the current working directory for grunt.
         */
        globalConfigurations = {
            'paths': cwd + '/config/.pathsrc',
            'jshintrc': cwd + '/config/.jshintrc',
            'csslintrc': cwd + '/config/.csslintrc',
            'requirejs': cwd + '/config/build.js'
        };

        // load path/glob configuration and save it
        config = JSON.parse(fs.readFileSync(globalConfigurations.paths, 'utf-8'));
        config.path.source.base = config.path.source.base.replace(/^(\.\/|\.)?(.*)/, cwd + '$2');

        distDirectory = config.path.build.base;
    }());

    return function (grunt) {

        /**
         * get all glob patterns for type into an array
         *
         * @param from
         * @param type
         * @returns {*|Array}
         */
        function getSources(from, type) {
            var sources = [],
                files;

            files = createFileObject( from, type);

            _.each( files, function (value){

                var src, path;

                for(var i = 0, ln = value.src.length; i < ln; i++ ){

                    src  = value.src[i];
                    path = value.cwd;

                    if (  src.match (/^!/) ) {
                        src  = src.replace(/^(!)?(.*)$/,'$2');
                        path = '!' + path;
                    }

                    sources.push ( path + '/' + src );
                }
            });

            return sources;
        }

        /**
         * returns path for specific type like css, js or image
         * concatenated with base path from source/build paths
         *
         * @private
         * @param {object} from
         * @param {string} type
         * @returns {string}
         */
        function getPath(from, type) {
            var path = _.trim(from[type]);

            // if path is not set or empty return base path
            if (!path || path.length === 0) {
                return from.base;
            }

            return from.base + '/' + path;
        }
        
        /**
         * returns path for specific type like css, js or image
         * concatenated with base path from source/build paths
         *
         * @private
         * @param {string} basepath
         * @param {array|*} from
         * @returns {array}
         */
        function getPathArray (basepath, from) {
            var pathArray = [],
                path;
            
            if ( _.isArray(from) ) {
                
                for(var i = 0, ln = from.length; i < ln; i+=1 ){
                    path = basepath + '/' + from[i];
                    pathArray.push(path);
                }
            }
            return pathArray;
        }

        /**
         * create grunt dest-as-target file format
         *
         * @private
         * @param source
         * @param target
         * @returns {object}
         */
        function bundleFiles(source, target) {
            var bundle = {};
            bundle[target] = source;

            return bundle;
        }

        function getTemplateFiles () {
            var obj = {},
                sourcePath = getPath( config.path.source, 'js'),
                targetPath = getPath( config.path.build, 'js'),
                sources, source, target,
                pattern = new RegExp ('^' + sourcePath.replace(/(\/|\\)/g, "\\/") + '\\/([^\\.]+).*$');
                
            sources = glob.sync(sourcePath + '/' + '**/*.hbs');
                
            if ( sourcePath && targetPath ) {
                
                /** 
                 * we need to execude glob pattern for every one 
                 */
                for(var i = 0, ln = sources.length; i < ln; i++ ) {
                    source = sources[i];
                    target =  source.replace( pattern, targetPath + '/$1.js');
                    obj[target] = source;
                }
            }
            return obj;
        }
                
        /**
         * create grunt Files Array Format with dynamic mappings
         *
         * @private
         * @param from
         * @param type
         * @returns {Array}
         */
        function createFileObject(from, type) {
            var result = [],
                i = 0, ln,
                self = this,
                excludePaths = [],
                tmpFileObjects = [];

            if (!from) {
                return result;
            }

            if (type) {

                if (from[type]) {
                    var sources = _.clone( from[type] );
                    
                    /**
                     * extract all object definitions to define extra file object
                     */
                    for(ln = sources.length; i < ln; i++ ) {

                        if ( _.isPlainObject( sources[i] ) ) {
                            
                            /**
                             * habe ich eigene files angegeben werden diese genommen 
                             */
                            var tmpSource = sources.splice(i, 1)[0],
                                sourcePath = getPath( config.path.source, type),
                                needlePath = getPath( config.path.source, tmpSource.path || type);

                            if ( tmpSource.subPath ) {
                                needlePath += '/' + tmpSource.subPath;
                            }
                            
                            if ( grunt.file.doesPathContain(sourcePath, needlePath) ) {
                                
                                var exclude = needlePath.replace(
                                    new RegExp('^' + sourcePath.replace(/(\\|\/)/g, '\\/') + '\/'),
                                    ''
                                );
                                excludePaths.push( "!" + exclude + '/*' );
                            }

    						var dest = getPath( config.path.build, type);

    						// if option keepPath is set the file will hold his path on copy 
    						// and not copied into root type path
    					   	if ( tmpSource.keepPath ) {
    							dest = [getPath(config.path.build, tmpSource.path ), tmpSource.subPath].join('/'); 
    						} 

                            result.push({
                                 src: tmpSource.files || sources,
                                 cwd: needlePath,
                                 dest: dest,
                                 expand: true
                            });
                            
                            ln--;
                            i--;
                        }
                    }
                    
                    result.push({
                        src: sources.concat( excludePaths || [] ),
                        cwd: getPath(config.path.source, type),
                        dest: getPath(config.path.build, type),
                        expand: true
                    });
                }

            } else {
                var keys = Object.keys( from );

                for(i = 0, ln = keys.length; i < ln; i++ ){
                    result = result.concat(createFileObject(from, keys[i]));
                }
            }
            return result;
        }
    
        /**
         * load grunt modules
         */
        grunt.loadNpmTasks('grunt-contrib-cssmin');
        grunt.loadNpmTasks('grunt-contrib-clean');
        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-csslint');
        grunt.loadNpmTasks('grunt-contrib-jshint');
        grunt.loadNpmTasks('grunt-contrib-requirejs');
        grunt.loadNpmTasks('grunt-contrib-handlebars');
        grunt.loadNpmTasks('grunt-contrib-compass');
        grunt.loadNpmTasks('grunt-contrib-sass');

        /**
         * grunt initial config
         */
        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json')
        });

        /**
         * check dist folder exists and if not create it
         */
        if (!grunt.file.isDir(distDirectory)) {
            grunt.file.mkdir(distDirectory);

            /* check again and if it fails end grunt
             */
            if (!grunt.file.isDir(distDirectory)) {
                grunt.fail.fatal('Distribution directory does not exists or cannot created ' + distDirectory);
            }
        }

        /**
         * Grunt checks dist path is in actual current working directory
         * if this is not the case change current working directory, this
         * is needed for clear task, which cannot by default delete files out
         * of the working directory.
         *
         * so set the current working directory to dist path ( config.path.build.dist )
         * and change all source / config paths to absolute paths.
         */
        if (!grunt.file.isPathInCwd(config.path.build.base)) {
            // set cwd to dist folder
            grunt.file.setBase(config.path.build.base);

            // reset build path, because this is now the working directory
            config.path.build.base = '.';
        }

        grunt.config.merge({

            /**
             * grunt clean
             */
            clean: {
                all: [getPath(config.path.build, 'base') + '/*'],
                css: [getPath(config.path.build, 'css') + '/**/*.css'],
                js: [getPath(config.path.build, 'js') + '/**/*.js'],
                images: [getPath(config.path.build, 'images') + '/**/*.{png,jpg,jpeg,gif,svg}'],
                fonts: [getPath(config.path.build, 'fonts') + '/*'],
                template: [getPath(config.path.build, 'template') + '/**/*.hbs']
            },

            compass: {
                'dev': {
                    options: {
                        sassDir: getPath(config.path.source, 'sass'),
                        cssDir: getPath(config.path.source, 'css'),
                        noLineComments: false
                    }
                },
            },

            copy: {

                /**
                 * copy all files js, css, images and fonts to dist folder
                 */
                all: {
                    files: createFileObject(config.files)
                },

                /**
                 * copy all js files we need to dist folder
                 */
                js: {
                    files: createFileObject(config.files, 'js')
                },

                /**
                 * copy all css files we need to dist folder
                 */
                css: {
                    files: createFileObject(config.files, 'css')
                },

                /**
                 * copy all image files we need to dist folder
                 */
                images: {
                    files: createFileObject(config.files, 'images')
                },

                /**
                 * copy all font files we need to dist folder
                 */
                fonts: {
                    files: createFileObject(config.files, 'fonts')
                },

                /**
                 * copy all html files we need to dist folder
                 */
                template: {
                    files: createFileObject(config.files, 'template')
                },

                /**
                 * copy all vendor files we need to dist folder
                 */
                vendor: {
                    files: createFileObject(config.files, 'vendor')
                }
            },

            /**
             * check css
             */
            csslint: {

                strict: {
                    options: {
                        csslintrc: globalConfigurations.csslintrc,
                        import: false
                    },
                    src: getSources(config.files, 'css')
                }
            },

            /**
             * minimize css
             */
            cssmin: {

                options: {
                    debug: true,
                    relativeTo: distDirectory,
                    root:  cwd,
                    keepBreaks: false
                },

                /**
                 * minimize and concat all css files and copied to dist
                 */
                combine: {
                    files: bundleFiles(
                        // source
                        getSources(config.files, 'css'),

                        // target
                        getPath(config.path.build, "css") + "/main.css"
                    )
                },

                /**
                 * minimize all css files by its own and copied to dist
                 */
                single: {
                    files: createFileObject(config.files, 'css')
                }
            },

            /**
             * code quality for js files
             */
            jshint: {

                options: {
                    'jshintrc': globalConfigurations.jshintrc,
                    'reporter': require('jshint-stylish')
                },

                strict: getSources(config.files, 'js')
            },

            handlebars: {
                'compile': {
                    options: {
                        'amd': 'handlebars',
                        'namespace': false,
                        'processContent': function (content) {
                            content = content
                                .replace(/[\r\n]+/gm, '')
                                .replace(/>\s+</gm, '><')
                                .replace(/\s+/gm, ' ');
                            
                            return content;
                        }
                    },
                    files: getTemplateFiles()
                }
            },
            
            requirejs: {
                compile: {
                    options: {
                        dir: getPath(config.path.build, 'js'),
                        appDir: getPath(config.path.source, 'js'),
                        baseUrl: '.',
                        mainConfigFile: getPathArray( getPath(config.path.source, 'config'), config.buildtools.requirejs.config),
                        fileExclusionRegExp: /\.hbs$/,
                        keepBuildDir: true
                    }
                }
            }
        });

        /**
         * combined task for debug mode
         * all files will copied without compression
         */
        grunt.task.registerTask('build', [
            'compass:dev',
            'jshint:strict',
            'clean:all',
            'cssmin:combine',
            'copy:js',
            'copy:images',
            'copy:fonts',
            'copy:vendor',
            'handlebars:compile'
        ]);

        /**
         * combined task for production mode
         * css files will compressed and concatenated.
         * js files will be compressed by its own and not concatenated to one file
         * copy only image and font files to dist folder
         */
        grunt.task.registerTask('production', [
            'compass:dev',
            'jshint:strict',
            'clean:all',
            'cssmin:combine',
            'copy:images',
            'copy:fonts',
            'copy:vendor',
            'handlebars:compile',
            'requirejs:compile'
        ]);
    };
}

module.exports = GruntConfig();
