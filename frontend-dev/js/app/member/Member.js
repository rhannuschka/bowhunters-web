/**
 * Member Plugin
 * @TODO should only be a PaginationPanel
 */
define('member/Member', [
    'jquery',
    'lib/AppCache',
    'view/PaginationPanel'
], function ($, AppCache, PaginationPanel) {

    return $.widget('bh.member', PaginationPanel, {
        /**
         *
         */
        _bodyTpl: 'member/template/index',

        options: {
            title: 'Mitglieder des Vereins'
        },

        initializeView: function ( options ) {

            var me = this,
                _super = this._superApply,
                initialized = $.Deferred();

            me._getMember().then( function ( response ) {

                if ( response.success ) {
                    me._setPageCount(response.data.pages);
                    me._setItemsOnPage(12);
                    me._setItemsTotal(response.data.total);
                    me._addData('member', response.data.member );
                }
            })
            .always( function () {
                /**
                 * let the parent do the work now
                 * it is unclear it do some async calls so wrap this
                 * into a promise
                 */
                $.when(_super.call( me, options))
                    .always( initialized.resolve());
            });
            return initialized.promise();
        },

        _getMember: function (params) {

            var data = $.extend({}, params, {
                action: AppCache.get('app.ajax.api.member.read'),
            });

            return $.ajax({
                url: AppCache.get('app.ajax.url'),
                type: 'GET',
                dataType: 'json',
                data: data
            });
        },

        /**
         * load a page
         * @param {int} page
         */
        _loadPage: function (page) {

            var me = this;

            me._getMember({
                page: page,
                sort: {
                    direction: 'asc',
                    field: 'name'
                }
            })
            .then ( function(response) {
                if ( response.success ) {
                    me._updateData('member', response.data.member);
                    me._renderBody();
                }
            });
        }
    });
});
