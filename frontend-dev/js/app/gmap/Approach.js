/*global google*/
define('gmap/Approach', [
    'jquery',
    'view/Panel',
    'lib/AppCache'
],
    function ($, Panel, AppCache) {

        var Approach = {

            options: {
                title: 'Kommen sie uns besuchen'
            },

            _maps: null,

            _bodyTpl: 'gmap/template/container',

            _init: function() {
                this._initMap();
                this._super();
            },

            _initMap: function () {

                var lat = AppCache.get('google.maps.bowhunters.latitude'),
                    lng = AppCache.get('google.maps.bowhunters.longitude');

                this.getBody().height(600);

                this._maps = new google.maps.Map(this.getBody().get(0), {
                    'center': {lat: lat, lng: lng},
                    'zoom': 16
                });

                this._maps = new google.maps.Marker({
                    position: {
                        lat: lat,
                        lng: lng
                    },
                    map: this._maps,
                    title: AppCache.get('google.maps.bowhunters.label')
                });
            }
        };

        return $.widget('gmaps.approach', Panel, Approach);
    }
);
