define([
    'config/app',
    'jquery',
    'service/WidgetService'
], function (config, jquery, WidgetService) {
    
    return {
        'run': function () {
            WidgetService.load( jquery('body') );
        }
    };
});
