define('guestbook/Form', [
 'jquery', 
 'view/Form'
],
    function ( $, FormView ){

        return $.widget('bh.guestbook_form', FormView, {

            options: {
                title: 'Noch nicht eingetragen ?',

                collapsible: 'xs',

                collapsed: false
            },

            widgetEventPrefix: 'guestbook_form:',

            _bodyTpl: 'guestbook/template/form',

            _submitButton: null,

            _lockView: null,

            initializeView: function () {
                this.addBodyClass('form');
                return this._superApply( arguments );
            },

            afterRender: function (form) {
                this._superApply(arguments);
                this._submitButton = form.find('[data-form-submit="form-'+this._getId()+'"]');
                this._lockView = form.find('[data-form-lock="form-'+this._getId()+'"]');

                this._lockView.hide();
            },

            _create: function () {
                
                var me = this;

                me._super();

                me._on( me.element, {
                    'guestbook_form:submit': $.proxy( me._onSubmitForm, me),
                    
                    'guestbook_form:clearerror': $.proxy( me._clearError, me),
                });
            },

            _onSubmitForm: function (event, _data_) {
                var data,
                    me = this;

                event.stopPropagation();
                data = this._normalizeData(_data_);
            
                me._trigger('insertdata', null, [data]);
            },

            _normalizeData: function (_data_) {
                var normalizedData = {};

                for ( var i = 0, ln = _data_.length; i < ln; i++) {
                    normalizedData[_data_[i].name] = _data_[i].value;
                }
                return normalizedData;
            },

            lock: function () {
                var me = this;
                
                me._submitButton.css('visibility', 'hidden');
                me._lockView.show();
                me._super();
            },

            /** 
             * @TODO add canvas to display form is locked
             * a rotating image 
             */
            unlock: function () {
                var me = this,
                    _super = me._super;

                this._displayLock( 1500, function () {
                    me._submitButton.css('visibility', 'visible');
                    me._lockView.hide();
                    _super.apply(me);
                });    
            },

            _displayLock: function ( time, done) {
                var me = this,
                    x = 10, 
                    y = 10,
                    startAngle = 3/2 * Math.PI,
                    lineWidth  = 2,
                    radius = 9,
                    context, 
                    startTime;

                context = me._lockView.get(0).getContext("2d");

                function draw () {

                    var endAngle,
                        _break = false,
                        timeLeft = (new Date().getTime() - startTime);

                    endAngle = startAngle - timeLeft * 2 * Math.PI / time;

                    if ( endAngle <= -0.5 * Math.PI ) {
                        endAngle = -0.5 * Math.PI;
                        _break = true;
                    }

                    context.clearRect(0, 0, 20, 20);
                    context.beginPath();
                    context.arc(x, y, radius, startAngle, endAngle, true);
                    context.lineWidth = lineWidth;
                    context.strokeStyle = '#06C724';
                    context.stroke();
            
                    if ( !_break ) {
                        window.requestAnimationFrame (draw);
                    } else {
                        context.clearRect(0, 0, 20, 20);
                        done();
                    }
                }
                startTime = new Date().getTime();
                draw();
            }
        });
    }
);
