/**
 * GuestbookController
 * @TODO documentation
 */
define('guestbook/Guestbook', [
    'jquery',
    'jquery.ui',
    'view/Container',
    'lib/AppCache'
], function ($, $ui, Container, AppCache) {
    
    return $.widget('bh.guestbook', Container, {

        /**
         *
         */
        _widgetEventPrefix: 'bh_guestbook:',

        /**
         *
         */
        _viewTpl: 'guestbook/template/index',

        /**
         *
         */
        _page: null,

        /**
         *
         */
        _form: null,

        /**
         *
         * @private
         */
        _create: function () {
            
            var me = this;

            this._form  = this._getWidget('[data-view="guestbook_form"]');
            this._page = this._getWidget('[data-view="guestbook_page"]');

            this._on( this.element, {
                'guestbook_form:insertdata' : $.proxy(me._onInsertData, me)
            });
        },

        /**
         * get widget from element
         *
         * @param {string} selector
         * @returns {*|jQuery}
         * @private
         */
        _getWidget: function (selector) {

            var widgetName,
                widget;

            widgetName = $(this.element).find(selector).first().data('widgetName');
            widget = $(':data("'+widgetName+'")', this.element)
                .first()
                .data(widgetName);

            return widget;
        },

        /**
         *
         * @param {jQuery} event
         * @param {object} data
         * @private
         */
        _onInsertData: function (event, data) {

            var me = this;

            event.stopPropagation();

            me._form._trigger('clearError');
            me._form.lock();

            this._saveData(data)
                .then( function success () {

                    // lock formular to prevent spam
                    me._form.clear();
                    me._page.reload();
                }, 
                function fail (error) {
                    me._form._displayErrors(error);
                }). 
                always( function () {
                    me._form.unlock();    
                });
        },

        /**
         *
         * @param data
         * @returns {jQuery.Deffered}
         * @private
         */
        _saveData: function (data) {
            
            var deferred = $.Deferred();

            $.ajax({
                type: 'POST',
                url: AppCache.get('app.ajax.url'),
                dataType: 'json',
                data: {
                    action: AppCache.get('app.ajax.api.guestbook.create'),
                    entry: data
                }
            }).done( function (response) {
                
                if ( response.success ) {
                    deferred.resolve( response || {});
                } else {
                    deferred.reject( response.error || {});
                }
            });
            
            return deferred.promise();
        }
    });
});
