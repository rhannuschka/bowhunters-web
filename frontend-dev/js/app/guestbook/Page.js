define('guestbook/Page',[
    'jquery',
    'view/PaginationPanel',
    'lib/AppCache'
], function ($, Paginate, AppCache) {
    
    return $.widget('rh.guestbook_page', Paginate, {
        
        options: {
            title: 'G&auml;stebuch'
        },

        widgetEventPrefix: 'guestbook_page:',
        
        _bodyTpl: 'guestbook/template/page',
        
        /** 
         * @params {object} options widget options
         */
        initializeView: function ( options ) {
            
            var initialized = $.Deferred(),
                me = this,
                _super = this._superApply;
                
            me._getEntries().then( function ( response ) {

                // preload guestbook data
                if ( response.success ) {
                    
                    me._setPageCount( response.data.pages );
                    me._setItemsOnPage( response.data.display );
                    me._setItemsTotal( response.data.total );

                    // add guestbook specific content
                    me._addData('entries', response.data.items);
                }

            }, function() {
                me._addData('entries', [{
                    'author': 'Ooooops!',
                    'message': '<h4>Technische Störung</h4>' +
                        'Wir sind uns des Problemes bewusst und arbeiten bereits daran.<br />' +
                        'Bitte Entschuldigen sie die Unanehmlichkeiten.'
                }]);
            }).always( function () {

                // call parent and ensure all is done before we resolve
                // initialisation
                $.when( _super.call(me, options) )
                    .always( initialized.resolve() );
            });
            return initialized.promise();
        },

        _getEntries: function (params) {

            var data = $.extend({}, params, {
                action: AppCache.get('app.ajax.api.guestbook.read'),
            });

            return $.ajax({
                url: AppCache.get('app.ajax.url'),
                type: 'GET',
                dataType: 'json',
                data: data 
            });
        },

        _loadPage: function (page) {
               
             var me = this;             

             me._getEntries ({
                page: page    
             }).then(function (response) {

                if ( response.success ) {
                    me._updateData('entries', response.data.items);
                    me._renderBody();
                }
             });
        },

        reload: function () {

            var me = this;

            me._getEntries().then( function (response) {
                if ( response.success ) {
                    me._updateData('entries', response.data.items);
                    me._updatePageCount( response.data.pages );
                    me._updateItemsOnPage( response.data.display );
                    me._updateItemsTotal( response.data.total );
                    me._updatePage();
                }
            });
        }
    });
});
