define('view/Panel', [
'jquery', 
'view/Container',
'service/TemplateService'
],
function ( $, Container, TemplateService) {
    
    return $.widget('rh.panel', Container, {

        options: {
            /**
             * panel title
             */
            title: 'Boring PanelHeader',

            /**
             * panel is collapsible 
             * possible values
             * false = allways displayed
             *  true = all devices
             *    sm = tablets
             *    xs = smartphones
             */
            collapsible: false,

            /**
             * true if panel is collpased , in this case 
             * collapsible will be true 
             * 
             * @param boolean 
             */
            collapsed: false
        },

        _viewTpl: 'template/panel',

        _bodyTpl: null, 

        _headerTpl: 'template/header',
        
        _body: null,

        _btnCollapse: null,

        _css: {},

        __construct: function () {

            this._css =  {
                'body': ['panel-body'],
                'header': ['panel-heading'],
                'main': ['panel']
            };
            this._superApply(arguments);
        },

        initializeView: function (options, element) {

            if ( this.options.collapsed ) {
                this.options.collapsible = true;
                this.addBodyClass('hidden');
            }

            // will be added in handlebars
            this._addData('.', {
                /** 
                 * header data
                 */
                'header': {
                    'title': this.options.title,
                    'class': this._css.header.join(' ')
                }, 
                /**
                 * panel body data
                 */
                'body': {
                    'class': this._css.body.join(' ')
                },
                /**
                 * generell data
                 */
                'collapsible': this.options.collapsible,
                      'class': this._css.main.join(' ')
            });

            // add templates 
            this._addTemplate('header.template', this._headerTpl);
            this._addTemplate('body.template'  , this._bodyTpl);
            this._addTemplate('template'       , this._viewTpl);

            return this._superApply( arguments );
        },

        _afterRender: function () {
            

            this._superApply(arguments);
        },

        _create: function () {
            var me = this;
            
            if ( this.options.collapsible ) {

                this._btnCollapse = this.element.find(
                    '[data-panel-section="header-'+this._getId()+'"] '+
                    '[data-trigger="collapse"]'
                );

                this._btnCollapse
                    .on ('click', $.proxy(me._onTriggerCollapse, me) );
            }

            me._super();
        },

        getBody: function () {

            if ( !this._body ) {
                this._body = this.element.find('[data-panel-section="body-'+this._getId()+'"]');
            }
            return this._body; 
        },

        _appendClass: function (_class_) {

            if ( $.type(_class_) !== 'string') {
                return;
            }

            if ( this.indexOf(_class_) < 0 ) {
                this.push(_class_);    
            }
        },

        /**
         *
         * @private
         */
        _onTriggerCollapse: function ( event, element) {

            event.stopPropagation();

            if ( this.getBody() ) {

                this.getBody().toggleClass('hidden');
                this._btnCollapse.toggleClass('open');

                //if ( !this.getBody().is(':visible') ) {} 
            }
        },

        _renderBody: function () {

            var me = this;

            if ( me._bodyTpl ) {
                me.update(me._bodyTpl, me.getBody());
            }
        },

        showBody: function () {
        },

        hideBody: function () {
        },

        addHeaderClass: function (_class_) {
            this._appendClass.call( this._css.header, _class_);
        },

        addBodyClass: function (_class_) {
            this._appendClass.call( this._css.body, _class_);
        },

        addPanelClass: function (_class_) {
            this._appendClass.call( this._css.main, _class_);
        }
    });
});
