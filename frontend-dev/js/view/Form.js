define('view/Form',[
    'jquery',
    'view/Panel'
], function ($, Panel) {
    
    return $.widget('bh.bh_form', Panel, {
        
        options: {
            title: 'Noch nicht eingetragen ?',

            bodyClass: 'form'
        },
        
        widgetEventPrefix: 'bh_form:',

        _locked: false,
        
        _bodyTpl: 'template/form/index',
        
        _errorViewTpl: 'template/form/error',
        
        _form: null,
        
        _errorView: null,

        /**
         *
         * @returns {*}
         */
        initializeView: function () {
            /**
             * add template with key error
             * we can resolve this via this.getTemplate('form.error');
             */
            this._addTemplate('form.error', this._errorViewTpl);
            return this._superApply( arguments );
        },

        /**
         *
         * @private
         */
        _create: function () {
            
            var me = this;
        
            me._form = me.element
                .find('form[data-form="form-' + me._getId('id')  + '"]');
            
            /**
             * create events
             */
            me._on(me.element, {
                
                'submit': $.proxy( me._handleFormSubmit, me),
                
                'clear': $.proxy( me.clear, me)
            });
            
            me._errorView = me.getTemplate('form.error');
            me._super();
        },

        /**
         *
         * @param event
         * @param form
         * @private
         */
        _handleFormSubmit: function (event) {
            
            var me = this;
            
            event.stopPropagation();
            event.preventDefault();
            
            if ( !this._locked ) {
                me._trigger('submit', null, [me._form.serializeArray()]);
            }
        },

        /**
         *
         * @param event
         * @private
         */
        clear: function () {
            this._form.trigger('reset'); 
        },

        /**
         * @param {String:Object} errorMessages
         * @private
         */
        _displayErrors: function (errorMessages) {
            
            var me = this,
                errorView;
            
            errorView = $(me._errorView(errorMessages)); 
            
            me.element.find('[data-form-view="error"]').remove();
            errorView.insertBefore(me._form);
        },

        _clearError: function ( event ) {
            this.element.find('[data-form-view="error"]').remove();
        },

        lock: function () {
            if ( this._submitButton ) {
                this._submitButton.prop('disabled', true);
            }
            this._locked = true;
        }, 

        unlock: function () {
            if ( this._submitButton ) {
                this._submitButton.prop('disabled', false);
            }
            this._locked = false;
        }
    });
});
