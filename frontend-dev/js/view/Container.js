/**
 * base container widget
 *
 * @class Container
 * @namespace View
 * @requires jQuery
 * @requires Lib.Cache
 * @requires Service.TemplateService
 * @requires Service.WidgetService
 * @constructor
 */
define(
'view/Container',
[
    'jquery',
    'service/TemplateService',
    'service/WidgetService',
    'lib/Cache'
],
function ( $, TemplateService, WidgetService, Cache) {

    return $.widget('rh.Container', {

        options: {
            parentscope: null,

            view: null,

            autorender: true
        },

        /**
         * parsed handlebars template which is the viewport for the container
         *
         * @property _viewTpl
         * @type String
         * @default "template/container"
         * @required
         * @private
         */
        _viewTpl: 'template/container',

        /**
         * parsed handlebars template which is the viewport for
         * the container
         *
         * @property _templatesToLoad
         * @type Object
         * @private
         */
        _templatesToLoad: {},

        /**
         * container specifc cache for templates, css classnames, container id and
         * additional data
         *
         * @property _cache
         * @type Lib.Cache
         * @private
         */
        _cache: null,

        /**
         * constructor function for widgets
         *
         * @param {Object} options
         * @param {jQuery} element
         * @private
         * @override
         */
        _createWidget: function (options, element) {

            var me = this,
                args = arguments,
                initialized;

            this.__construct(options);

            initialized = this.initializeView( options );

            if ( !initialized ) {
                initialized = true;
            }

            $.when( initialized )
                .then ( function () {

                    if ( !me._viewTpl ) {
                        return false;
                    }
                    // load main template
                    return TemplateService.load([me._viewTpl]);
                })
                .then ( function () {
                    // render main template
                    return me._doRender( element );
                })
                .then ( function (rendered) {
                    $.Widget.prototype._createWidget.apply(me, args );

                    // trigger event to child widgets attached
                    // we not really trigger an event just call a method
                    if ( element.closest('body').length ) {
                        WidgetService.trigger('attached', WidgetService.getWidgets(element) );
                    }
                });
        },

        __construct: function (options) {

            this._templatesToLoad = {};
            this._cache = new Cache( options.parentscope );
            this._cache.add('id', Math.random().toString(36).substr(2, 9) );
        },

        initializeView: function (options) {},

        /**
         * renders element only called once before widget _create is fired
         * this is initial rendering for widget and all child widgets.
         *
         * @param {jQuery} element
         */
        _doRender: function (element) {
            var me = this,
                tplData = me._getData(),
                rendered;

            return me._loadTemplates()
                .then( function () {

                    me.beforeRender(tplData);
                    // render main template
                    rendered = TemplateService
                        .render(me._viewTpl, tplData);

                    /**
                     * search child widgets and initialize them
                     */
                    return WidgetService
                        .load(rendered, tplData);
                })
                .then(function(childWidgets){
                    me.afterRender(rendered);
                    element.append(rendered);
                    return $.when();
                });
        },

        /**
         * should not called direcly, this loads all templates
         * and put them into cache.
         *
         * @private
         */
        _loadTemplates: function () {

            var me = this;

            return TemplateService
                .load( me._templatesToLoad )
                .then( function (templates) {
                    $.each( me._templatesToLoad, function(name, tpl) {
                        me._cache.add( name, templates[tpl]);
                    });
                    me._templatesToLoad = {};
                    return templates;
                });
        },

        /**
         * add template data
         *
         * @param {string} name Namespace where to save it divided by .
         * @param {object|*} value
         * @returns void
         */
        _addData: function (path, value) {
            this._cache.add(path, value);
        },

        _getData: function( _path_, extractFullPath ) {

            var path = _path_ || '.';
            return this._cache.get(path, extractFullPath);
        },

        /**
         * update template data
         *
         * @param {string} name Namespace where to save it divided by .
         * @param {object|*} value
         * @returns void
         */
        _updateData: function (path, value) {
            this._cache.update(path, value);
        },

        /**
         * removes data which was saved in template
         * so we can update this
         *
         * @param {string} namespace what we want to remove
         * @returns void
         */
        _clearData: function (_path_) {

            var path = _path_;

            if ( !path ) {
                path = '.';
            }
            this._cache.remove(path);
        },

        _getId: function () {
            return this._cache.get('id');
        },

         /**
         * add css classes
         */
        addClass: function (_path_, _class_, update) {

            var me = this,
                value,
                path;

            path = _path_ || '.';

            if ($.type(_class_) === 'array') {
                value = _class_.join(' ');
            } else if ($.type(_class_) == 'string' || $.type(_class_) == 'object') {
                value = _class_;
            } else {
                return false;
            }

            if ( me._cache.get(path) ) {
                if ( update ) {
                    me._cache.update(path, value);
                }
            } else {
                me._cache.add(path, value);
            }
        },

        /**
         * update a container template if no template is given
         * the full container will be rendered.
         *
         * @param {String|Object} tpl
         * @param {jQuery} el
         */
        update: function (tpl, el) {
            var me = this,
                template = me._viewTpl,
                element = me.element,
                rendered;

            if (tpl && el) {
                template = tpl;
                element = el;
            }

            if (template) {
                rendered = TemplateService.render( template, me._getData());
                WidgetService
                    .load(rendered, me._getData() )
                    .then( function () {
                        WidgetService.destroy(WidgetService.getWidgets(element));
                        element.html(rendered);
                        WidgetService.trigger('attached', WidgetService.getWidgets(element));
                    });
            }
        },

        beforeRender: function () {},

        afterRender: function ( fragment ) {},

        /**
         * @param {string} name
         * @param {string} tpl template path
         * @returns void
         */
        _addTemplate: function (name, tpl ) {
            if ( !tpl ) {
                tpl = name;
            }
            this._templatesToLoad[name] = tpl;
        },

        getTemplate: function ( name ) {
            return this._cache.get(name);
        }
    });
});
