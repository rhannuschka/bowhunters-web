define('view/PaginationPanel', [
'jquery', 
'view/Panel'
],
function ( $, Panel) {
    
    return $.widget('extjq.panel_pagination', Panel, {
        
        _viewTpl: 'template/paginate',
        
        widgetEventPrefix: 'rh_PaginationPanel:',
        
        /**
         * @name _body
         * @type {Array|jQuery} pagination widgets from panel
         */
        _paginationWidgets: null,
        
        /**
         * @name _body
         */
        _body: null,
        
        /**
         * @name _elementSelector
         * @type {object|string}
         */
        _elementSelector: {},
        
        /**
         * aktuelle Seite
         *  
         * @name currentPage
         * @type {int}
         */
        currentPage: 0,    
 
        __construct: function () {
            this._superApply(arguments);

            this._elementSelector = {
                pagination: '[data-pagination="'+this._getId()+'"]'
            };

            this.addPanelClass('pagination-panel');
        },
        
        getPaginationWidgets: function (){
            return this._paginationWidgets;
        },

        initializeView: function ( options ) {

            if ( !this._getData('pagination.count') ) {
                this._setPageCount(0);
            }

            if ( !this._getData('pagination.itemsOnPage') ) {
                this._setItemsOnPage(0);
            }

            if ( !this._getData('pagination.total') ) {
                this._setItemsTotal(0);
            }

            return this._superApply(arguments);
        },

        afterRender: function (fragment) {
            this._paginationWidgets = $(fragment).find( this._elementSelector.pagination );
            this._superApply( arguments );
        },

        /**
         * @private
         * @returns void
         */
        _create: function () {
            var me = this;
            
            this._on( this.element, {
                'pagination:pageselect': $.proxy( me._handlePageSelect, me)
            });
        },

        _setPageCount: function (count) {
            if ( count && $.type( count ) == 'number' ) {
                this._addData('pagination.count', count);  
            }
        },

        _setItemsOnPage: function (itemsOnPage) {
            if ( (itemsOnPage || itemsOnPage === 0) && $.type( itemsOnPage ) == 'number' ) {
                this._addData('pagination.itemsOnPage', itemsOnPage);  
            }
        },

        _setItemsTotal: function (total) {
            if ( (total || total === 0)  && $.type(total) == 'number' ) {
                this._addData('pagination.total', total);  
            }
        },

        _updatePageCount: function (count) {
            if ( count && $.type( count ) == 'number' ) {
                this._updateData('pagination.count', count);  
            }
        },

        _updateItemsOnPage: function (itemsOnPage) {
            if ( (itemsOnPage || itemsOnPage === 0) && $.type( itemsOnPage ) == 'number' ) {
                this._updateData('pagination.itemsOnPage', itemsOnPage);  
            }
        },

        _updateItemsTotal: function (total) {
            if ( (total || total === 0)  && $.type(total) == 'number' ) {
                this._updateData('pagination.total', total);  
            }
        },
        
        /**
         * @name _handlePageSelect
         * @param {jQuery} event
         * @param (int} pagination the selected pagination
         * @param {jQuery} element
         * @private
         * @description 
         * handle paginationselect event from lib/Pagination
         */
        _handlePageSelect: function (event, page, element) {
            
            var widget, 
                elements, 
                me = this;

            // check event was triggered from own pagination widgets
            // it this is not the case ignore event and bubble up
            if ( this._paginationWidgets.is( element ) ){
            
                event.stopPropagation();
                
                // remove element which has triggered event from list
                // and notify all other pagination widgets which are a part 
                // from pagination panel.
                elements = this._paginationWidgets; // .not( element );

                $.each( elements, function ( index, element) {
                    widget = $(element).data('widget');
                    widget.setSilent( true );
                    widget.setPage(page);
                    widget.setSilent( false );
                });
                me._loadPage(page);
            }
        },

        _loadPage: $.emptyFn,

        _updatePage: function () {
            
            var widget,
                me = this,
                data = me._getData('pagination');

            $.each( this._paginationWidgets, function ( index, element) {
                widget = $(element).data('widget');
                widget.setSilent( true );
                widget.updatePage(data.total);
                widget.setSilent( false );
            });
            me._renderBody();
        },
    });
});
