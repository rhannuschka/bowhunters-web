requirejs.config({

    // base url where to load data from
    baseUrl: wp_appconfig.paths.js,
    
    paths: {
        'config': 'config',
        
        'vendor': './../vendor',
        
        'handlebars.helper': 'vendor/handlebars/viewhelper',

        'requirejs-plugins': './../vendor/requirejs-plugins',

        'guestbook': 'app/guestbook',

        'member': 'app/member',

        'gmap': 'app/gmap',

        /**
         * requirejs plugins
         */
        'text': './../vendor/text/text'
    },
    
    map: {
        '*' : {
            'jquery.pagination': 'lib/jquery.plugins/jquery.simplePagination',
            
            'handlebars': 'vendor/handlebars/handlebars.min',

            'viewhelpers': 'viewhelper/index',

            'async': 'requirejs-plugins/src/async'
        },
    },

    shim: {
        'lib/jquery.plugins/jquery.simplePagination': ['jquery']
    }
});
