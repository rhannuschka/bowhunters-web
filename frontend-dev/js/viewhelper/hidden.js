/**
 * create css class like hidden-xs-up or hidden-md-down 
 * @see bootstrap 4 alpha responsive utilitys
 * 
 * {{hidden 'xs|sm|md|lg' 'up|down'}}
 * 
 * @TODO should be changed for bootstrap 4
 */
define(
    'viewhelper/hidden', 
    ['handlebars', 'jquery'],
    function ( Handlebars, $) {

        Handlebars.registerHelper('hidden', function (hidden, direction, options) {
            
            var hiddenMode = ['xs', 'sm', 'md', 'lg'],
                  cssClass = ['hidden'],
                   context,
                   display,
                       rtn;

            if (hidden) { 
            
                if ($.type(hidden) == 'string') {
                    display = hiddenMode.indexOf(hidden) > -1 ? 0 : hiddenMode.indexOf(hidden);
                }
            
                // class will now be hidden-{{mode}}
                cssClass.push( hiddenMode[display] );

                if ( direction && $.type(direction) === 'string') {

                    if ( direction == 'up' && hiddenMode[display+1] ) {
                        cssClass.splice(1, 1, hiddenMode[display+1]);
                    }

                    // class will now become hidden-{{mode+n}}-{{direction}}
                    cssClass.push(direction);
                }

                context = {
                    'class': 'hidden ' + cssClass.join('-')
                };

                rtn = options.fn(context);
            } else {
                rtn = options.inverse(hidden);
            }
            return rtn;
        });
    }
);
