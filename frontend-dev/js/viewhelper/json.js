define('viewhelper/json', ['handlebars'], function (Handlebars) {
    Handlebars.registerHelper('json', function (context) {
        return JSON.stringify(context);
    });
});
