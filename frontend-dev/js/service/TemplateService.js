/**
 * @TemplateService v 1.1
 */
define([
    'jquery',
    'handlebars',
    'lib/Cache',
], function ($, Handlebars, Cache) {
    
    var instance,
        templateCache;
    
    function TemplateService () {
        
        templateCache  = new Cache();

        function requestTemplates ( templates, done) {
            
            var requestedTemplates = [];
            
            $.each( templates, function(key, value){
                requestedTemplates.push( value );
            });
            requirejs( requestedTemplates, done);
        }
        
        /**
         * @param {array|string|object} templates to load
         */
        function load(template) {
            
            var required = [],
                resolved = {},
                data,
                deferred = $.Deferred();
            
            data = normalize(template);
                
            $.each(data, function(name, tplPath) {
                
                if ( !getTemplate(tplPath) ) {
                    required.push(tplPath);
                } else {
                    resolved[tplPath] = getTemplate(tplPath);
                }
            });

            if (required.length) {
                
                // argumente sind die ganzen templates die geladen wurden
                // als JS oder Text je nachdem
                requestTemplates( required, function () {
                    var name,
                        tpl;
                    
                    $.each( arguments, function (key, value) {
                        name = required.splice(0, 1)[0];
                        tpl  = value;
                        
                        // compile template 
                        if ( $.type(tpl) != 'function' ) {
                            tpl = Handlebars.compile(tpl);
                        }

                        templateCache.add( name, tpl);
                        resolved[name] = tpl;
                    });
                    deferred.resolve( resolved );
                });
            } else {
                deferred.resolve(resolved);
            }
            return deferred.promise();
        }

        function normalize (_data_ ) {

            var templateData = {};

            switch ($.type(_data_) ) {
                
                case 'array': 
                    $.each( _data_, function (index, value ) {
                        templateData[value] = value;
                    });
                    break;

                case 'string': 
                    templateData[_data_] = _data_;
                    break;

                case 'object':
                    templateData = _data_;
                    break;
            }

            return templateData;
        }

        function getTemplate (path) {
            return templateCache.get(path);
        }

        function render(name, viewModel){
            var rendered;
            if ( getTemplate(name) ) {
                /**
                 * render template with data 
                 */
                try {
                    rendered = $(getTemplate(name)(viewModel));
                } catch ( e ) {

                    var error = new Error();
                    error.message = 'template could not be rendered';
                    error.template = name;
                    error.templateData = viewModel;
                    error.stack = e;
                    throw error;
                }
            }
            return rendered;
        }

        /**
         * updates a single template, after rendering initialize all included widgets 
         *
         * @TODO implement
         * @param {string} template
         * @param {object|*} data 
         * @return deferrerd.promise
         */
        function update (template, data) {
            return render( template, data);    
        }
        
        return {
            load: load,
            get: getTemplate,
            render: render,
            update: update
        };
    }
    
    TemplateService.getInstance = function () {
        if ( !instance ) {
            instance = new TemplateService();
        }
        return instance;
    };
    
    return TemplateService.getInstance();
});
