define([
    'jquery'
], function ($) {
    'use strict';

    var instance;

    function WidgetManager () {
        function getWidgets($start) {
            var widgetNodes;
            widgetNodes = $start.find('[data-widget]').addBack('[data-widget]');
            return $.makeArray(widgetNodes)
                .map(function(node) {
                    return getWidget($(node));
                });
        }

        function trigger(event, widgets, params){
            $.each( widgets, function(index, widget) {
                // trigger attached event on all child widgets
                widget._trigger(event);
            });
        }

        function destroy(widgets){
            $.each( widgets, function(index, widget) {
                widget.destroy();
            });
        }

        /**
         *
         */
        function getWidget($element){
            return $element.data('widget');
        }

        /**
         * Initialization of all widgets with the selector '[data-widget]'.
         * @param {*|HTMLElement|jQuery} $startElement  - (selector of) the starting node; default is "html"
         * @param {Object} parentscope , will be pass to widget
         * @public
         */
        function load($startElement, parentscope) {

            var $start = $startElement || $('html'),
                $widgetElements = $start.find('[data-widget]').addBack('[data-widget]'),
                get, need,
                childWidgetsReady = $.Deferred();

            if ($widgetElements.length) {

                get = 0;
                need = $widgetElements.length;

                $widgetElements.each(function () {
                    var $elem = $(this),
                        name = $elem.data("widget"),
                        options = $elem.data();

                    if ( $.trim(name) !== '' ) {

                        requirejs([name], function (_widget_) {
                            var widget,
                                created = $.Deferred();

                            $.extend( options, {
                                /**
                                 * if all child widgets are created
                                 * resolve promise, so all child widgets are ready to use
                                 */
                                'create': function (event) {

                                    //$elem.data('widget', widget);
                                    //$elem.data('widget-name', widget.widgetFullName );
                                    created.then(function(){
                                        get++;

                                        if ( get >= need ) {
                                            childWidgetsReady.resolve();
                                        }
                                    });
                                }
                            });

                            /**
                             * check parent is instance of container
                             * in this case this is a container widget and become
                             * parent viewmodel scope injected as option
                             */
                            if ( _widget_.prototype instanceof $.rh.Container ) {
                                $.extend( options, {
                                    parentscope: parentscope || {}
                                });
                            }

                            // create the widget instance
                            widget = _widget_(options, $elem);
                            $elem.data('widget', widget);
                            $elem.data('widget-name', widget.widgetFullName);
                            created.resolve(widget);
                        });
                    }
                });

            } else {
               childWidgetsReady.resolve([]);
            }

            return childWidgetsReady.promise();
        }

        /**
         * Public interface.
         * @public
         */
        return {
            load: load,
            getWidgets: getWidgets,
            getWidget: getWidget,
            trigger: trigger,
            destroy: destroy
        };
    }

    WidgetManager.getInstance = function () {
        if ( !instance ) {
            instance = new WidgetManager();
        }
        return instance;
    };

    return WidgetManager.getInstance();
});
