define('boot', 
['config/common', 'lib/AppCache', 'config/app'], 
function(config, AppCache, AppConfig){

    /**
     * define jQuery
     */
    define('jquery', function () {
        if ( jQuery ) {
            return jQuery.noConflict();
        }
        return null;
    });

    /**
     * jQuery-ui
     */
    define('jquery.ui', function () {

        if ( jQuery.ui ) {
            jQuery.extend( jQuery.Widget.prototype, {
                _silent: false,
                isSilent: function () {
                    return this._silent;
                },
                setSilent: function ( silent ) {
                    this._silent = !!silent;
                    return this;
                }
            });
            return jQuery.ui;
        }
        return null;
    });

    // clone wordpress config and add it to cache
    AppCache.add('app.ajax', jQuery.extend(true, {},  wp_appconfig.ajax, AppConfig) );

    // load files first
    requirejs([
        'app/main',
        'viewhelper/index',
        'jquery',
        'jquery.ui',
        'async!https://maps.googleapis.com/maps/api/js?key=' + AppCache.get('google.apikey')
    ], function (main) {
        /**
         * start main app
         */
        main.run();
    });
});
