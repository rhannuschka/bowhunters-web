define('lib/PaginationRenderer', 
[
    'jquery',
    'jquery.pagination'
],
    function ($, Pagination) {

        function PaginationRenderer (maxentries, opts) {
            $.PaginationRenderers.defaultRenderer.apply( this, arguments);
        }

        PaginationRenderer.prototype = Object.create($.PaginationRenderers.defaultRenderer.prototype, {
            
            updateMaxEntries: function (_maxentries_) {
                this.pc.maxentries = _maxentries_;
            }
        });
        return PaginationRenderer;
    }
);
