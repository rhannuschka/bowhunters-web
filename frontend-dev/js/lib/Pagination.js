define([
'jquery', 
'jquery.pagination'
],
function ($, pagination) {
    
    return $.widget('extjq.rhPagination', {
        
        options: {
            displayedPages: 3,
               itemsOnPage: 10,
                itemsTotal: 0,
                     edges: 1
        },

        widgetEventPrefix: 'pagination:',

        _create: function () {
            var element = $(this.element),
                me = this;

            element.pagination({
                items: me.options.itemsTotal,
                itemsOnPage: me.options.itemsOnPage,
                edges: me.options.edges,
                displayedPages: me.options.displayedPages,
                listStyle: 'pagination',
                nextText: '&raquo;',
                prevText: '&laquo;',
                selectOnClick: false,
                onPageClick: function ( page ) {
                    me._pageSelected( page, element);
                    return false;
                }
            });
        },
        
        _pageSelected: function (page, element) {
            if ( !this.isSilent() ){
                this._trigger('pageselect', null, [page, element]);
            }
        },

        setPage: function (page) {
            this.element.pagination('selectPage', page);
            this.element.pagination('redraw');
        },

        updatePage: function (itemsTotal) {
            this.element.pagination('updateItems', itemsTotal);
            this.element.pagination('selectPage', 1);
            this.element.pagination('redraw');
        }
    });
});
