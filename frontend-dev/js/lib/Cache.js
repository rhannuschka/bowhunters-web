define(
[], 
function () {
    'use strict';
        
    function Cache ( prototype ) {
    
        /**
         * @TODO ensure prototype is an object and not a string or anything else
         */
        var _cache = Object.create(prototype||{});

        /**
         *  
         */
        function getNamespace (_path_) {

            var namespace = null, 
                parts,
                name, 
                object;

            namespace = _path_;
               object = _cache;
                parts = namespace.split('.');

            if ( !_path_.match(/^\.$/ ) ) {

                while (parts.length > 0) {
                    name = parts.splice(0, 1)[0];

                    if ( object.hasOwnProperty(name) && isObject(object[name]) ){
                        object = object[name];  
                        continue;
                    }

                    // set to null, because this can not be a valid namespace
                    object = null;
                    break;
                }
            }
            
            /**
             * if path not exists in map
             * cache was not created yet
             */
            return object;
        }
    
        /**
         * proof if given source is from type Object
         * 
         * @param {object} obj to check  
         * @returns boolean 
         */
        function isObject (needle) {
            return Object.prototype.toString.call(needle).match(/([^\s]+)\]$/)[1].toLowerCase() == 'object';
        }

        /**
         * @private
         * @params {string} path 
         * @returns {object|*} the parsed informations
         */
        function parsePath(path) {

            var info = {
                property: '',
                namespace: '.'
            },
            /**
             * Rückgabe des Patterns ist
             * 
             * parsed[2] = der Name der Variable
             * parsed[1] = Namespace oder undefined
             */
            parsed = path.match(/^(?:(.*)\.)?([^\.]+)$/);
            
            if ( parsed ) {
                info.property  = parsed[2];
                info.namespace = parsed[1] || '.';
            }
            return info;
        }        

        /**
         * create a new namespace if it is possible, 
         * a namespace is just like a directory so 
         * if path is set by any other value else as an 
         * object it will fail.
         *
         * @returns {mixed|*}
         */
        function createNamespace (_path_, start) {
        
            var object,
                name,
                namespace,
                parts;
                
            object = start || _cache;

            namespace = _path_;
            parts = namespace.split('.');

            while (parts.length > 0) {
                name = parts.splice(0, 1)[0];

                if ( !object.hasOwnProperty(name) ) {
                    object[name] = {};
                } 

                if ( isObject(object) ) {
                    object = object[name];
                    continue;
                }

                object = null;
                break;
            }
             
            return object;
        }

        /**
         * insert json data in cache, this will only called on addValue
         * 
         * @params {object} json data to add
         * @params {object} start where to insert
         * @private
         */
        function readFromJSON (json, start) {

            var cacheObject;
               
            // loop json data
            for(var key in json) {
                   
                if ( json.hasOwnProperty( key ) ) {
                    cacheObject = start || _cache;
                    // if we found nested json go in 
                    // create a new cache path or get existing 
                    // and loop recursivly call readFromJSON 
                    if ( isObject(json[key]) ) { 
                        cacheObject = createNamespace(key, cacheObject);

                        if ( cacheObject ) {
                            readFromJSON(json[key], cacheObject) ;
                        }
                        continue;
                    } else {
                        
                        // it is a blank value like String, Integer or Array
                        // in this case we save the value into cache 
                        if ( !cacheObject.hasOwnProperty( key )) {
                            cacheObject[key] = json[key];    
                        }
                    }
                }
            }
        }
        
        /**
         * add new value to cache, if allready a value exists for path
         * property wont be writte @see update
         *
         * @property [{string} path] to property
         * @property {mixed} value
         */
        function addValue(path, value) {

            var info,
                cache = _cache;

            // parse path
            // info.namespace where to add
            // info.property which name
            info = parsePath(path);

            if ( info.namespace !== '.' ) {
                cache = getNamespace(info.namespace) || createNamespace(info.namespace);
            } 

            // if value is an Object we need to call readFromJSON
            if ( cache ) {

                if ( isObject(value) ) {
                    readFromJSON(value, cache[info.property] || _cache);
                }

                if ( info.property.length && !cache.hasOwnProperty(info.property) ) {
                    cache[info.property] = value;
                }
            } 
        }

        /**
         * @TODO implement better logic
         * 
         * @param {string} path where to find the property
         */
        function getValue(path, extractPath) {
        
            var info = parsePath ( path ),
                value,
                extract,
                namespace;

            namespace = _cache;
                value = _cache;

            if ( info.namespace !== '.' ) {
                namespace = getNamespace(info.namespace);     
            }

            if ( info.property.length && namespace ) {
                value = namespace[info.property];

                if ( extractPath ) {
                    var obj = {},
                        partials = info.namespace.split('.');

                    extract = obj;

                    for(var i = 0,  ln = partials.length; i < ln; i++) {
                        obj = obj[partials[i]] = {};
                    }

                    obj[info.property] = value;
                    value = extract;
                }
            }
            return value;
        }

        /**
         * delete a value from cache
         *
         * @param {string} path which path to delete
         */
        function deleteValue(path) {
            var info = parsePath ( path ),
                namespace,
                deleted = false;

            namespace = _cache;

            if ( info.namespace.length ) {
                namespace = getNamespace(info.namespace);     
            }
        
            if ( namespace && namespace.hasOwnProperty(info.property) ) {
                delete namespace[info.property];
                deleted = true;
            }

            return deleted;
        }

        /**
         * update a value in cache, this will delete old value
         * and then rewrite value
         * 
         * @param {string} path where we find the property with current value 
         * @param {mixed} value the new value
         */
        function updateValue( _path_, _value_) {

               /*jshint validthis: true */
            var path = _path_,
                 val = _value_, 
                  me = this;

            if ( me.remove(path) ) {
                me.add(path, val);
            }
        }

        return {
            add: addValue,
            get: getValue,
            remove: deleteValue,
            update: updateValue
        };
    }

    return Cache;
});
