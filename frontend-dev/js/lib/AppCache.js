define(['lib/Cache'], function (Cache) {
    'use strict';

    var instance,
        cache;

    function AppCache () {

        /**
         * create Cache instance
         */
        cache = new Cache();

        function addValue(path, value) {
            cache.add.apply(cache, arguments);
        }

        function getValue(path, value) {
            return cache.get.apply(cache, arguments);
        }

        function updateValue(path, value) {
            cache.update.apply(cache, arguments);
        }

        return {
            add: addValue,
            get: getValue,
            update: updateValue
        };
    }
    
    AppCache.getInstance = function () {
        if ( !instance ) {
            instance = new AppCache();
        }
        return instance;
    };

    return AppCache.getInstance();
});

