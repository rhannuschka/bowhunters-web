<?php
    $menu = get_query_var('menutitle');
?>
<sidebar class="panel col-sm-4 col-md-3">
    <nav class="sidebar sidebar--menu">
        <?php wp_nav_menu( array('menu' => $menu)); ?>
    </nav>
</sidebar>
