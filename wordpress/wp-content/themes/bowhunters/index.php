<?php get_header()?>
<div id="main" class="row">
    <?php while(have_posts()): the_post()?>
    <article class="col col-sm-12">
        <h2><?php the_title()?></h2>
        <?php the_content(__('Continue Reading'))?>
    </article>
    <?php endwhile;?>
</div>
<?php get_footer()?>
