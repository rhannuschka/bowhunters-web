            <div id="footer-push"></div>
        </div>
        <footer id="footer" class="page-footer container">
            <div class="col col-md-4">
                <ul>
                    <li>Impressum</li>
                    <li>Sitemap</li>
                    <li>Kontakt</li>
                </ul>
            </div>
            
            <div class="col col-md-8">
                <p>
                    Bowhunters Teuchern <br />
                    Weißenfelser Straße 18 <br />
                    06682, Teuchern <br />
                </p>
                <address>
                    <a href="https://www.facebook.com/pages/Bowhunter-Teuchern/">www.facebook.com/bowhunter.teuchern</a>
                </address>
            </div>
        </footer>
        <?php wp_footer(); ?>
    </body>
</html>
