<?php
/**
 * get all plugins we got installed for bowhunters
 */
if ( !function_exists('get_plugins') ) {
    require_once ABSPATH . 'wp-admin/includes/plugin.php';
}

function bowhunters_script_enqueue() {

    $pluginPath = '/bowhunters/';
    $all_plugins = get_plugins($pluginPath);

    if (!is_admin()) {

        wp_enqueue_style('bowhunters.css', get_template_directory_uri() . '/assets/css/main.css', array(), '', 'all' );

        wp_deregister_script('jquery');
        wp_deregister_script('jqueryui');
        
        wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/vendor/jquery/dist/jquery.js', array(), '2.1.4', false );
        wp_enqueue_script('jqueryui', get_template_directory_uri() . '/assets/vendor/jquery-ui/jquery-ui.js', array(), '1.11.4', false );

        // add requirejs to footer
        wp_enqueue_script('requirejs', get_template_directory_uri() . '/assets/vendor/requirejs/require.js', array(), '2.1.20', true);

        foreach($all_plugins as $plugin => $meta ) {
            require_once WP_PLUGIN_DIR . $pluginPath . $plugin;
            try {
                $name = substr($plugin, 0, strrpos($plugin, '.'));
                $api[ strtolower($name) ] = call_user_func('Bowhunters_' .$name. '::getAjaxApi');
            } catch (Exception $exception) {}
        }

        /**
         * write wordpress js
         */
        $appData = array(
            'paths' => array(
                'js' => get_template_directory_uri() . '/assets/javascripts/'
            ),
            'ajax' => array(
                'url' => admin_url ('admin-ajax.php'),
                'api'  => $api
            )
        );
        wp_localize_script( 'requirejs', 'wp_appconfig', $appData);
    }
}

add_filter( 'script_loader_tag', function ( $tag, $handle) {

    switch ($handle ) {
        case 'requirejs':
            $parts = preg_split('/\s/', $tag);
            array_splice(
                $parts,
                1, 0,
                array(
                    'data-main="'.get_template_directory_uri().'/assets/javascripts/boot.js"',
                    'defer'
                )
            );
            $tag = implode(" ", $parts);
            return $tag;
        default:
            return $tag;
    }
}, 10, 2);

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

add_action( 'wp_enqueue_scripts', 'bowhunters_script_enqueue');
