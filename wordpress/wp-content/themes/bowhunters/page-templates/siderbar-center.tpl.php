<?php
/*
Template Name: Sidebar-Center
*/
global $wp_query;

$sidebarmenu = get_post_meta($wp_query->post->ID, 'sidebarmenu', true);
set_query_var('menutitle', $sidebarmenu);

/**
 * widget in the center
 */
$widgetName = get_post_meta($wp_query->post->ID, 'widget', true);
$widgetPath = null;
if($widgetName && trim($widgetName) !== '' ) {
    $widgetPath = $widgetName . '/' . ucfirst($widgetName);
}
get_header();
?>
<div id="contentwrap" class="row">

    <?php get_template_part('partials/sidebar', 'menu'); ?>

    <div class="panel content-container col-sm-8 col-md-9"
        <?php if($widgetPath) :?>
            data-widget="<?php echo $widgetPath; ?>">
        <?php else : ?>
            >
        <?php endif; ?>
        <?php
            echo $wp_query->post->post_content;
         ?>
    </div>
</div>
<?php get_footer()?>
