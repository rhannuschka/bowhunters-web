<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <meta charset="utf-8" />
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
        <?php wp_head() ?>
    </head>
    <body>
        <div class="container wrapper" id="layout">

            <header id="header" class="top-header row">
                <nav id="mainnav" class="main-navigation col col-md-12" >
                    <?php wp_nav_menu( array( 'menu' => 'mainnav')); ?>
                </nav>
            </header>
