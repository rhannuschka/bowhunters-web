<?php get_header()?>
<div>
    <?php while(have_posts()): the_post()?>
        <article class="row">
            <h2><?php the_title()?></h2>
            <?php the_content()?>
        </article>
    <?php endwhile;?>
</div>
<?php get_footer()?>
