<?php
/*
Plugin Name: Bowhunters Mitglieder
Plugin URI: www.bowhunters.de
Description: Mitglieder Liste 
Version: 0.0.1
Author: Ralf Hannuschka 
Domain Path: /lang/
*/
class Bowhunters_Member {

    private $db_name;

    public function __construct () {
    
        if ( ! is_admin() || defined('DOING_AJAX') && DOING_AJAX ) {
            require_once('member/frontend/controller/IndexController.php');
            new  Bowhunters_Member_Frontend_Controller_IndexController();
        } else {
            register_activation_hook( __FILE__, array($this, 'install'));
            require_once('member/admin/controller/IndexController.php');
            new  Bowhunters_Member_Admin_Controller_IndexController();
        }
    }
        
    public static function getAjaxApi () {
        
        return array(
            'read' => 'bowhunters_member_read'
        );
    }

    public function install () {

        global $wpdb;

        $db_name = $wpdb->prefix.'bowhunter_member';
        $db_name_role = $wpdb->prefix.'bowhunter_member_role';
        $db_charset = $wpdb->get_charset_collate();
       
        if ( $wpdb->get_var("SHOW TABLES LIKE '$db_name'" ) !== $db_name ) {
            
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            $query =  "CREATE TABLE $db_name (
                uid INT NOT NULL AUTO_INCREMENT,
                name VARCHAR(255) NOT NULL,
                lastname VARCHAR(255),
                role VARCHAR(255),
                bow VARCHAR(255), 
                description MEDIUMTEXT NOT NULL,
                image varCHAR(255),
                primary key (uid)
            ); $db_charset;"; 
            dbDelta($query);
        }

        $dummyData = array( 'name' => 'Ralf', 'lastname' => 'Hannuschka', 'role'=> 1, 'bow_brand' => 'Unikat', 'bow_class' => 'Recurve', 'description' => 'teste hier nur rum');

        for($i = 0, $ln = 30; $i < $ln; $i++) {

            $person = $dummyData;
            $wpdb->insert(
                'wp_bowhunter_member',
                array(
                    'name' => $person['name'],
                    'lastname' => $person['lastname'],
                    'role' => $person['role'],
                    'bow' => $person['bow'],
                    'description' => $person['description']
                ) 
            );
        }
    }
}
new Bowhunters_Member();
