<?php
class Bowhunters_Chronic_Admin_Bootstrap {

  const LIST_PAGE = 'bht-chronic/index';

  const EDIT_PAGE = 'bht-chronic/index/edit';

  const ADD_PAGE = 'bht-chronic/index/add';

  const CONFIG_PAGE = 'bht-chronic/config';

  public function __construct ()
  {
    // add menu structure
    add_action('admin_menu', array($this, 'createMenu'));
    $this->loadPageController();
  }

  public function createMenu ()
  {
    global $menu;
    global $submenu;

    $new_menu = array( 'BHT Chronics', 'manage_options', self::LIST_PAGE, 'bht_chronic', 'menu-top', '');
    $menu[] = $new_menu;

    $submenu[self::LIST_PAGE][] = array('Eintrag anlegen'   , 'manage_options', 'admin.php?'.self::ADD_PAGE);
    $submenu[self::LIST_PAGE][] = array('Eintrag bearbeiten', 'manage_options', 'admin.php?'.self::EDIT_PAGE);
    $submenu[self::LIST_PAGE][] = array('Konfiguration'     , 'manage_options', 'admin.php?'.self::CONFIG_PAGE);
  }

  public function loadPageController ()
  {
    var_dump($_SERVER['REQUEST_URI']);
  }
}
