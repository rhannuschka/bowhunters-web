<?php

require_once (WP_PLUGIN_DIR . '/bowhunters/chronic/admin/views/View.php');

class Bowhunters_Chronic_Admin_Controller_IndexController {

    const LIST_PAGE = 'bht-chronic/index';

    const EDIT_PAGE = 'bht-chronic/index/edit';

    const ADD_PAGE = 'bht-chronic/index/add';

    const CONFIG_PAGE = 'bht-chronic/config';

    private $view;

    public function __construct () {

        add_action('admin_menu', array($this, 'loadMenu'));
        add_action('admin_init', array($this, 'init'));
        add_action('admin_enqueue_scripts', array($this, 'loadScript'));

        $this->view = new Bowhunters_Chronic_Admin_View();
    }

    public function init () {
    }

    public function loadScript ($hook) {
    }


    public function activateStyle () {
      wp_enqueue_style('bowhunterMemberCSS');
      wp_enqueue_style('fontAwesomeCSS');
    }

    public function indexAction ()
    {

      $this->view->setTemplate('templates/list.phtml');
      $this->view->set('title', 'Bowhunter Teuchern Chronik');
      $this->view->render();
    }

    public function addChronicAction ()
    {
    }

    public function editChronicAction ()
    {
    }

    public function configureChronicAction ()
    {
    }
}
