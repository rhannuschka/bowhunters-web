<?php
/*
Plugin Name: Bowhunters Chronic
Plugin URI: www.bowhunters.de
Description: Mitglieder Liste
Version: 0.0.1
Author: Ralf Hannuschka
Domain Path: /lang/
*/
class Bowhunters_Chronic {

  private $db_name;

  private $isAdmin;

  public function __construct () {
    $this->isAdmin = is_admin() && !(defined('DOING_AJAX') && DOING_AJAX);

    if ( $this->isAdmin ) {
      register_activation_hook( __FILE__, array($this, 'install'));
    }

    // register hook for request
    add_action( 'init', array($this, 'onInitBhtChronic') );
  }

  public function onInitBhtChronic ($request)
  {
    if ( !$this->isAdmin ) {
      require_once('chronic/frontend/controller/IndexController.php');
      new  Bowhunters_Chronic_Frontend_Controller_IndexController();
    } else {
      parse_str( parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY), $parsedQuery );
      require_once('chronic/admin/Bootstrap.php');
      new Bowhunters_Chronic_Admin_Bootstrap($parsedQuery);
    }
  }

  public static function getAjaxApi () {
    return array(
      'read' => 'bowhunters_chronic_read'
    );
  }

  public function install () {
    global $wpdb;

    $db_name = $wpdb->prefix.'bowhunter_chronic';
    $db_charset = $wpdb->get_charset_collate();

    if ( $wpdb->get_var("SHOW TABLES LIKE '$db_name'" ) !== $db_name ) {
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
    }
  }
}
new Bowhunters_Chronic();
