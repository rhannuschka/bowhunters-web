(function($){

    /**
     * Bht-Fileupload jQuery Plugin 
     *
     * simple fileupload plugin for jQuery
     * required childelemnets 
     * 
     * img with data attribute bht-fileupload="preview"
     * input:file with data attribute bht-fileupload="file"
     * 
     * @example 
     * <fieldset ... data-plugin="bht-fileupload">
     *     <div> 
     *         <img ... data-bht-fileupload="preview" />
     *     </div>
     * 
     *     <input type="file" ... data-bht-fileupload="file" />
     * </fieldset>
     *
     * @TODO refactor 
     */
    $.fn.bhtFileupload = function (options) {

        var fileinput,
            filepreview,
            fileReader,
            fileChanged,
            fileRemoveBtn,
            fileDeleteCheckbox,
            defaultFile,
            currentFile,
            element;

        $element = this;

        /**
         * initialize plugin
         */
        (function () {

            var prefix = 'bht-fileupload',
                valid = true,
                options;

            options = $element.data();

            filepreview = $('#' + options.filePreview );
              fileinput = $('#' + options.fileUpload );

            valid = filepreview && filepreview.length;
            valid = valid && fileinput && fileinput.length;

            if ( !valid ) {
                throw new Error("bhtFileupload PLugin is not valid");
            }

            fileRemoveBtn = $('<button class="bh-btn" type="button" role="reset-uploaded-image"><i class="icon file-remove"></i></button>');
            fileDeleteCheckbox = $('<input type="checkbox" name="delete-file" />'); //.hide();

            fileReader = new FileReader();
            defaultFile = options.defaultImage;
            currentFile = options.currentImage;
            fileChanged = false;
            registerEvents();
            initFilePreview();
        }());

        function initFilePreview () {

            if ( currentFile != defaultFile ) {
                filepreview.before(fileRemoveBtn);
            }
        }

        /**
         * register events
         */
        function registerEvents() {

            var form;

            fileinput.on({ 'change': handleFileChange });
            fileReader.onload = handleFileLoadEvent;

            form = fileinput.closest('form');
            form.on({'reset': resetFile});
            form.append(fileDeleteCheckbox);

            // reset only uploaded file
            fileRemoveBtn.on({'click': deleteFile});
        }

        /**
         * handle event file input field was changed 
         * this will so we read the uploaded file into the FileReader 
         * and make a preview
         */
        function handleFileChange () {
            var file;

            file = fileinput.prop('files');
            fileReader.readAsDataURL(file[0]);

            return false;
        }

        function resetFile(event) {
            var image = currentFile || defaultFile;
            

            if ( fileChanged ) {
                fileChanged = false;
                fileinput.val(null);
            }

            if ( image !== defaultFile ) {
                filepreview.before(fileRemoveBtn);
            } else {
                fileRemoveBtn.detach();
            }

            fileDeleteCheckbox.prop('checked', false);
            filepreview.attr('src', image);
        }

        /** 
         *
         */
        function deleteFile (event) {

            var image = defaultFile;

            fileDeleteCheckbox.prop('checked', false);

            // we have uploaded an image
            if ( fileChanged ) {
                resetFile(event);
                return;
            }

            if ( currentFile !== defaultFile ) {
                fileDeleteCheckbox.prop('checked', true);
                // set flag file is deleted
                filepreview.attr('src', defaultFile);
            }
            fileRemoveBtn.detach();
        }

        /**
         * handle file was uploaded to browser, not to server 
         */
        function handleFileLoadEvent (event) {

            if ( !fileChanged ) {
                filepreview.before(fileRemoveBtn);
            }

            fileDeleteCheckbox.prop('checked', false);
            filepreview.attr('src', event.target.result);
            fileChanged = true;
        }
    };

    /**
     * BHT-CHECKBOX jQuery Plugin
     * simple plugin to check / uncheck all checkboxes with 1 click
     */
    $.fn.bhtCheckbox = function (options) {
        
        var checkboxes, 
            me = this;
        
        if (options.fieldset) {
            checkboxes = $(options.fieldset).find('input:checkbox');
        }

        me.on('change', function(event) {
            checkboxes.prop('checked', me.prop("checked") );
        });

        return this;
    };

    /**
     * jQuery Plugin Initializer
     * 
     * on document ready parse whole document for jQuery Plugins
     * and initialize them.
     * The selector is data-plugin="pluginName" all other data attribute will pass 
     * as option to plugin.
     */
    $(document).ready( function () {

        $('[data-plugin*="bht-"]').each(function ( index, element) {

            var $element, data, plugin;

            $element = $(element);
            data = $element.data();
            plugin = data.plugin;

            /**
             * convert my-substr-otherstr to mySubstrOtherstr
             */
            plugin = plugin.replace(/-(?=\w)(\w)/g, function (fullmatch, $1, strpos, str) {
                return $1.toUpperCase();
            });

            // remove plugin from data
            delete data.plugin;

            if (plugin) {
                $element[plugin](data);
            }

            $element.removeAttr('data-plugin');
        });
    });
}(jQuery));
