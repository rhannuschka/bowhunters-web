<?php
require_once (WP_PLUGIN_DIR . '/bowhunters/member/store/MemberStore.php');

class Bowhunters_Member_Frontend_Controller_IndexController {

    private $store;

    public function __construct () {
        $this->store = new Bowhunters_Member_Store_MemberStore();
        add_action( 'init', array($this, 'init') );
    }

    public function init () {
        add_action('wp_ajax_bowhunters_member_read'  , array( $this, 'read' ));
        add_action('wp_ajax_nopriv_bowhunters_member_read'  , array( $this, 'read' ));
    }

    public function read () {

        $memberList = array();
         $listStart = 0;
              $page = 1;

        if ( isset($_GET['page']) && is_numeric($_GET['page']) ) {
            $page = intval($_GET['page']);
        }

        if ($page && $page > 1) {
            $listStart = ($page-1) * 12;
        }

        $memberCount = $this->store->countMember();
        $members  = $this->store->listMember($listStart, 12);

        $pages = ceil($memberCount/10);
        $total = $memberCount;

        foreach($members as $member) {
            $tmp = array(
                'description' => $member['description'],
                      'image' => $member['image'],
                       'name' => $member['name'] . " " . $member['lastname'],
                       'role' => $member['role'], 
                        'bow' => $member['bow']
            );

            array_push($memberList, $tmp);
        }

        echo json_encode( array(
            'success' => true,
            'data' => array(
                'member' => $memberList,
                'pages' => $pages,
                'total' => $total
            )
        ));
        die();
    }
}
