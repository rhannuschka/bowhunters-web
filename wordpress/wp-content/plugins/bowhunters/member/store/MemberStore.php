<?php
require_once (WP_PLUGIN_DIR . '/bowhunters/member/model/User.php');

class Bowhunters_Member_Store_MemberStore {

    private $wpdb;

    const TABLE_NAME = 'bowhunter_member';

    public function __construct () {
        global $wpdb;
        $this->wpdb = $wpdb;
    }

    public function createMember($data = array(), $validate = true) {
        $member = new Bowhunter_Model_User();
        $member->setData($data, $validate);
        return $member;
    }

    public function getMember ( $userid ) {

        $query = "SELECT * 
                  FROM {$this->wpdb->prefix}".self::TABLE_NAME."
                  WHERE uid=". $userid;

        $result = $this->wpdb->get_row( $query, ARRAY_A);

        if( count($result) > 0 ) {
            $member = new Bowhunter_Model_User($result);
        }
        return $member;
    }

    public function countMember () {
        global $wpdb;
        
        $query = "SELECT COUNT(uid) AS total 
                  FROM {$this->wpdb->prefix}".self::TABLE_NAME;

        $results = $wpdb->get_row( $query, ARRAY_A, 0);
        return (int)$results['total'];
    }

    public function listMember ($start = 0, $limit = 20) {
        global $wpdb;
        
        $query = 'SELECT * 
                  FROM '. $this->wpdb->prefix.self::TABLE_NAME .'
                  LIMIT '. $start . ',' . $limit;

        $results = $wpdb->get_results( $query, ARRAY_A);
        return $results;
    }

    /**
     * @returns <int> last inserted id
     */
    public function add (Bowhunter_Model_User $member) {
        $data = array(
            'name' => $member->getName(), 
            'lastname' => $member->getLastname(),
            'role' => $member->getRole(),
            'bow' => $member->getBow(),
            'image' => $member->getImage(),
            'description' => str_replace( array("\r", "\n"), '', nl2br($member->getDescription()) ) 
        );
        $this->wpdb->insert($this->wpdb->prefix.self::TABLE_NAME, $data);
        return $this->wpdb->insert_id;
    }

    public function update (Bowhunter_Model_User $member) {
        $data = array(
            'name' => $member->getName(), 
            'lastname' => $member->getLastname(),
            'role' => $member->getRole(),
            'bow' => $member->getBow(),
            'image' => $member->getImage(),
            'description' => str_replace( array("\r", "\n"), '', nl2br($member->getDescription()) ) 
        );
        return $this->wpdb->update($this->wpdb->prefix.self::TABLE_NAME, 
            $data,
            array('uid' => $member->getUid())
        );
    }

    public function remove(Bowhunter_Model_User $member) {

        return $this->wpdb->delete(
            $this->wpdb->prefix.self::TABLE_NAME,
            array('uid' => $member->getUid())
        );
    }
}
