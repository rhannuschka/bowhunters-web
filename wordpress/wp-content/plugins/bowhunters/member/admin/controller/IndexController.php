<?php
require_once (WP_PLUGIN_DIR . '/bowhunters/member/store/MemberStore.php');
require_once (WP_PLUGIN_DIR . '/bowhunters/member/admin/views/View.php');
require_once (WP_PLUGIN_DIR . '/bowhunters/member/model/User.php');

class Bowhunters_Member_Admin_Controller_IndexController {

    private $store;

    private $view;

    const MEMBER_EDIT_PAGE = 'bht-member-edit';

    const MEMBER_ADD_PAGE = 'bht-member-add';

    const MEMBER_LIST_PAGE = 'bht-member';

    public function __construct () {

        add_action('admin_post_save_user', array($this, 'saveMember'));
        add_action('admin_action_member_delete', array($this, 'deleteMember'));
        add_action('admin_menu', array($this, 'loadMenu'));
        add_action('admin_init', array($this, 'init'));
        add_action('admin_enqueue_scripts', array($this, 'loadScript'));

        $this->store = new Bowhunters_Member_Store_MemberStore();
        $this->view = new Bowhunters_Member_Admin_View();
    }

    public function init () {
        wp_register_style('bowhunterMemberCSS', plugins_url('bowhunters/public/resources/backend/css/index.css'));
        wp_register_style('fontAwesomeCSS', plugins_url('bowhunters/public/resources/vendor/font-awesome-4.5.0/css/font-awesome.min.css'));
    }

    public function loadScript ($hook) {
        if (strpos($hook, 'page_'. self::MEMBER_LIST_PAGE) !== false) {
            wp_enqueue_script('bowhunterMemberJS', plugins_url('bowhunters/public/resources/backend/js/index.js'));       
        }
    } 

    public function loadMenu () {
        
        $page = add_menu_page( 'bhtmember', 'BHT Mitglieder', 'manage_options', self::MEMBER_LIST_PAGE, array($this, 'indexAction'));
        $addUser = add_submenu_page( 'bht-member', 'adduser', 'neues Mitglied', 'manage_options', self::MEMBER_ADD_PAGE, array($this, 'addMemberAction'));
        $editUser = add_submenu_page( null, 'edituser', 'Mitglied bearbeiten', 'manage_options', self::MEMBER_EDIT_PAGE, array($this, 'editMemberAction'));

        add_action("admin_print_styles-{$page}", array($this, 'activateStyle'));
        add_action("admin_print_styles-{$addUser}", array($this, 'activateStyle'));
        add_action("admin_print_styles-{$editUser}", array($this, 'activateStyle'));
    }

    public function activateStyle () {
        wp_enqueue_style('bowhunterMemberCSS');
        wp_enqueue_style('fontAwesomeCSS');
    }

    public function indexAction () {
        $listStart = 0;
        $page = 1;
        $users = array();

        delete_transient('bh_member_post_data');

        if ( isset($_GET['displayPage']) && is_numeric($_GET['displayPage'] ) ) {
            $page = (int)$_GET['displayPage'];
            $listStart = ($page - 1) * 15;
        }

        $memberCount = $this->store->countMember();

        $pages = paginate_links( array(
            'base' => add_query_arg('displayPage', '%#%'),
            'format' => '',
            'prev_text' => __( '&laquo;', 'text-domain'),
            'next_text' => __( '&raquo;', 'text-domain'),
            'total' => ceil($memberCount/15),
            'current' => $page  
        )); 

        foreach ($this->store->listMember($listStart, 15) as $memberData ) {
            $member = new Bowhunter_Model_User($memberData);
            array_push($users, $member);
        }

        $this->view->setTemplate('templates/list.phtml');
        $this->view->set('members', $users );
        $this->view->set('title', 'Mitgliederliste' );
        $this->view->set('pagination', $pages );
        $this->view->set('link_delete', admin_url("admin.php?action=member_delete"));
        $this->view->render();
    }


    public function addMemberAction () {

        $error = array();
        $member = $this->store->createMember();

        $this->view->setTemplate('templates/userform.phtml');
        $this->view->set('title', 'Neues Mitglied' );
        $this->view->set('actionUrl', admin_url("admin-post.php") );
        $this->view->set('error', $error);
        $this->view->set('error', $error);
        $this->view->set('link_list', admin_url("admin.php?page=".self::MEMBER_LIST_PAGE));
        $this->view->set('member', $member);
        $this->view->render();
    }

    /**
     * edit existing user
     */
    public function editMemberAction () { 

        $request = unserialize(get_transient('bh_member_post_data'));
        $error   = array();

        if ($request && count($request['error'])) {
            $member = $this->store->createMember($request['data'], false);
            $error  = $request['error'];
        } else if (isset($_GET['uid']) && is_numeric($_GET['uid']) ){
            $member = $this->store->getMember($_GET['uid']);
        }

        $this->view->setTemplate('templates/userform.phtml');
        $this->view->set('actionUrl', admin_url("admin-post.php") );
        $this->view->set('error', $error);
        $this->view->set('title', 'Mitglied bearbeiten' );
        $this->view->set('member', $member);
        $this->view->set('link_list', admin_url("admin.php?page=". self::MEMBER_LIST_PAGE));
        $this->view->render();
    }

    public function saveMember(){

        $member = $this->store->createMember($_POST);
        $transData = array(
            'error' => array(),
            'data' => array() 
        );

        /**
         * upload file
         */
        if ( !$member->hasErrors() ) {

            // upload image
            if ( isset($_FILES) && isset($_FILES['member_image_upload']) && $_FILES['member_image_upload']['error'] === 0) {
                $member->uploadImage($_FILES['member_image_upload']);
            } else if ( isset($_POST['delete-file']) ) {
                $member->removeImage();
            }

            if ($member->getUid()){
                $this->store->update($member);
            } else {
                $member->setUid( $this->store->add($member) ); 
            }

            delete_transient('bh_member_post_data');
        } else {
            $transData['error'] = $member->getErrors();
            $transData['data']  = $_POST; 

            // persist errors
            set_transient('bh_member_post_data', serialize($transData), 3600); 
        }       

        // do redirect
        wp_redirect(admin_url("admin.php?page=".self::MEMBER_EDIT_PAGE."&uid=".$member->getUid()));
        exit();
    }

    /**
     * delete a member
     */
    public function deleteMember(){

        $isMember = isset($_GET['uid']) && is_numeric($_GET['uid']);
        $isMember = $isMember && ($member = $this->store->getMember($_GET['uid']));

        if ($isMember && $this->store->remove($member)) {
            $member->removeImage();
        }
        wp_redirect(admin_url("admin.php?page=".self::MEMBER_LIST_PAGE."&uid=".$member->getUid()));
        exit();
    }
}
