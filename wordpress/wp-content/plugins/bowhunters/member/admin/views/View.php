<?php
class  Bowhunters_Member_Admin_View {

    private $template;

    private $data;

    public function __construct () {
        $this->data = array();
    }

    public function setTemplate($template = 'templates/list.phtml') {
        $this->template = $template;
    }

    public function set($name, $value) {
        $this->data[$name] = $value;
    }

    public function get($name) {
        return $this->data[$name];
    }

    public function render() {

        ob_start();
        require_once($this->template);
        ob_get_contents();       
        ob_flush();
    }
} 
