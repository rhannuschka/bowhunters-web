<?php

class Bowhunter_Model_User {

    private $name;

    private $id;

    private $lastname;

    private $role;

    private $bow;

    private $image;

    private $description;

    private $errorFields = null;

    const DEFAULT_IMAGE = 'bowhunters/public/resources/images/user-dummy.png';

    public function __construct ( $member = null, $validate = true) {
        $this->errorFields = array();

        if ($member) {
            $this->setData($member, $validate);
        }
    }

    public function setData($data, $validate){

        if ( isset($data) ) {

            foreach ($data as $key => $value) {
                $setter = 'set' . ucfirst($key);
                try {
                    if ( method_exists($this, $setter) ) {
                        $this->$setter($value, $validate);
                    }
                } catch ( Exception $e ) {
                    $this->errorFields[$key] = $value;
                }
            }
        }

        if ( !$this->getImage() ) {
            $this->image = plugins_url(self::DEFAULT_IMAGE);
        }
    }

    private function validateAlphaNumeric ($string) {

        $string = trim($string);        

        $valid = strlen($string) !== 0;
        $valid = $valid && preg_match('/^[\p{L}\p{N}_\s-]+$/u', $string); 
    
        if ( !$valid ) {
            throw new Exception("not valid: $string");
        }
        return $valid;
    }

    private function validateAlpha ($string) {

        $string = trim($string);        
        $valid = strlen($string) !== 0;
        $valid = $valid && preg_match('/^[\p{L}\s-_]+$/u', $string); 

        if ( !$valid ) {
            throw new Exception("Bitte geben sie einen gültigen Wert ein.");
        }
        return $valid;
    }

    public function hasErrors() {
        return !!count($this->errorFields);
    }

    public function wpseUploadDir($dirs) {

        $dirs['subdir'] = '/bht_member/images';
        $dirs['path']   = $dirs['basedir'] . '/bht_member/images';
        $dirs['url']    = $dirs['baseurl'] . '/bht_member/images';

        return $dirs;
    }

    public function removeImage() {

        if ($this->getImage() != plugins_url(self::DEFAULT_IMAGE)) {
            $upload_dir = wp_upload_dir();
            $file = $upload_dir['basedir'] . '/bht_member/images/' . basename($this->getImage());
            unlink($file);
            $this->setImage(plugins_url(self::DEFAULT_IMAGE));
        }
    }

    public function uploadImage($file) {
    
        if (!$file) {
            return;
        }

        // load file upload
        if ( !function_exists('wp_handle_upload') ) {
            require_once(ABSPATH . 'wp_admin_includes/file.php');
        }

        // before we upload anything remove existing file first
        $this->removeImage();

        add_filter('upload_dir', array($this, 'wpseUploadDir'));
        $upload_overrides = array('test_form' => false);
        $movefile = wp_handle_upload( $file, $upload_overrides); 
        remove_filter('upload_dir', array($this, 'wpseUploadDir'));

        if ( $movefile && !isset( $movefile['error'] ) ) {
            $this->setImage(
                wp_make_link_relative($movefile['url'])
            );
        }
    }

    /*===================================================
    /* GETTER
    /*=================================================*/

    public function getDefaultImage() {
        return plugins_url(self::DEFAULT_IMAGE);
    }

    public function getErrors() {
        return $this->errorFields;
    }

    public function getUid () {
        return $this->id;
    }

    public function getName () {
        return $this->name;
    }

    public function getFields () {
    
        return $fields;
    }

    public function getLastname () {
        return $this->lastname;
    }

    public function getRole () {
        return $this->role;
    }

    public function getImage () {
        return $this->image;
    }

    public function getBow () {
        return $this->bow;
    }

    public function getDescription () {
        return $this->description;
    }

    /*===================================================
    /* SETTER
    /*=================================================*/
    public function setUid ($id) {
        $this->id = (int)$id;
    }

    /**
     * @param {String} name
     */
    public function setName ($name, $validate = true) {

        if ( !$validate || $this->validateAlpha($name) ) {
            $this->name = $name;
        }
    }

    /**
     * @param {String} name
     */
    public function setLastname ($lastname, $validate = true) {

        if ( !$validate || $this->validateAlpha($lastname) ) {
            $this->lastname = $lastname;
        }
    }

    /**
     * @param {String} bow
     */
    public function setRole ($role, $validate = true ) {

        if ( !$validate || $this->validateAlpha($role) ) {
            $this->role = $role;
        }
    }


    public function setImage ($image) {
        $this->image = $image;
    }

    /**
     * @param {String} name
     */
    public function setBow ($bow, $validate = true) {

        if (!$validate || $this->validateAlphaNumeric($bow) ) {
            $this->bow = $bow;
        }
    }

    public function setDescription ($description, $validate = true) {
        $data = trim($description);
        $data = strip_tags($data, '<br></br>');
        $this->description = $data;
    }
}
