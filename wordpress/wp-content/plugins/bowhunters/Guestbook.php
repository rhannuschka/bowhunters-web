<?php
/*
Plugin Name: Bowhunters Guestbook
Plugin URI: 
Description: Der billige Versuch das gb_wolle gb halbwegs OOP zu machen
Version: 1.0.01
Author: Ralf Hannuschka 
Domain Path: /lang/
*/
class Bowhunters_Guestbook {
    
    private $error;
    
    private $entry;
    
    public function __construct () {
        add_action( 'init', array($this, 'init') );
    }    
    
    public function init () {
        
        $this->error = array();
        $this->entry = new gwolle_gb_entry();
        
        add_action('wp_ajax_bowhunters_guestbook_read'  , array( $this, 'read' ));
        add_action('wp_ajax_bowhunters_guestbook_create', array( $this, 'create'));
        
        add_action('wp_ajax_nopriv_bowhunters_guestbook_read'  , array( $this, 'read' ));
        add_action('wp_ajax_nopriv_bowhunters_guestbook_create', array( $this, 'create'));
        
    }
        
    public static function getAjaxApi () {
        
        return array(
            'create' => 'bowhunters_guestbook_create',
            'read' => 'bowhunters_guestbook_read'
        );
    }

    public function create () {
        
        $entry;
        $response;
        $formSetting;
        $entryId;
        
        $formSetting = gwolle_gb_get_setting( 'form' );
        
        $response = array(
            'success' => false,
            'error'   => [],
            'entry'   => []
        );
        
        if ( isset($_POST) && isset($_POST['entry']) && is_array($_POST['entry']) ){ 
            
            foreach( $_POST['entry'] as $key => $value ) {
                
                $validateFn = 'validate' . ucfirst($key);
                $required   = isset($formSetting['form_'.$key.'_mandatory']) && $formSetting['form_'.$key.'_mandatory'] === 'true';
                
                if (method_exists($this, 'validate' . ucfirst($key))) {
                    $this->$validateFn($value, $required);
                } else {
                    $this->error[] = array(
                        'field' => 'none',
                        'message' => 'ungültige Daten übermittelt'
                    );
                    break;
                }
            }
        
            if ( count($this->error) === 0 ) {
                
                // set success
                $response['success'] = true;
                
                /* Check for spam and set accordingly */
                $isspam = gwolle_gb_akismet( $this->entry, 'comment-check' );
                if ( $isspam ) {
                    // Returned true, so considered spam
                    $entry->set_isspam(true);
                    
                    // success is false
                    $response['success'] = false;
                    $this->error[] = 'Ihre Nachricht wurde als Spam erkannt ein Moderator wird dies prüfen.';
                }
                
                $this->beforeSave();

                if ( !($entryId = $this->entry->save()) ) {
                    $this->error[] = 'Speichern fehlgeschlagen.';
                    $response['success'] = false;
                }
                
                if ($response['success']) {
                    $this->entry->load($entryId);
                    
                    $data = array(
                        'author'  => $this->entry->get_author_name(),
                        'email'   => $this->entry->get_author_email(),
                        'website' => $this->entry->get_author_website(),
                        'message' => nl2br($this->entry->get_content())
                    );
                } else {
                    $data = array(
                        'author'  => 'Bowhunters Teuchern',
                        'website' => 'http://www.bowhunters.dev',
                        'message' => '<h3>Herzlich willkommen</h3><p>Es sind noch keine Einträge vorhanden und wir freuen uns auf Ihre Meinung.</p>'
                    );
                }
                $response['data'] = $data;
            }
        }
        $response['error'] = $this->error;
        
        echo json_encode($response);
        die();
    }
    
    public function read () {
        
        $page = 1;
        $pageCount = 1;

        if ( isset($_GET['page']) && is_numeric($_GET['page']) ) {
            $page = intval($_GET['page']);
        }

        $entriesPerPage = (int) get_option('gwolle_gb-entriesPerPage', 20);
        $entriesCount = gwolle_gb_get_entry_count(
            array(
                'checked' => 'checked',
                'trash'   => 'notrash',
                'spam'    => 'nospam',
                'book_id' => 1
            )
        );

        $countPages = ceil( $entriesCount / $entriesPerPage );

        if ( $page > $countPages ) {
            // Page doesnot exist
            $page = 1;
        }

        if ($page == 1 && $entriesCount > 0) {
            $firstEntryNum = 1;
            $mysqlFirstRow = 0;
        } elseif ($entriesCount == 0) {
            $firstEntryNum = 0;
            $mysqlFirstRow = 0;
        } else {
            $firstEntryNum = ($page - 1) * $entriesPerPage + 1;
            $mysqlFirstRow = $firstEntryNum - 1;
        }
        
        /* Get the entries for the frontend */
        $entries = gwolle_gb_get_entries(
            array(
                'offset'      => $mysqlFirstRow,
                'num_entries' => $entriesPerPage,
                'checked'     => 'checked',
                'trash'       => 'notrash',
                'spam'        => 'nospam'
            )
        );
        
        $data = array(
            "total" => $entriesCount,
            "display" => $entriesPerPage,
            "pages"=> $pageCount,
            "items" => array()
        );

        if ($entries ) {
            for($i = 0, $ln = count($entries); $i < $ln; $i++ ) {
                $entry = $entries[$i];
                array_push($data["items"], array(
                    'author'  => $entry->get_author_name(),
                    'email'   => $entry->get_author_email(),
                    'website' => $entry->get_author_website(),
                    'message' => nl2br($entry->get_content())
                ));
            }
        } else {
            array_push($data["items"], array(
                'author'  => 'Bowhunters',
                'website' => 'bowhunters.dev',
                'message' => '<h4>Es sind noch keine Einträge vorhanden</h4><p>Die Bowhunters Teuchern freuen sich auf Ihre Meinung.</p>'
            ));
        }

        echo json_encode( array(
            'success' => true,
            'data' => $data
        ));
        die();
    }

    private function beforeSave () {
        $this->isChecked();
    }
    
    private function isChecked () {
        
        $isChecked = false;

        /* if Moderation is off, set it to "ischecked" */
        $user_id = get_current_user_id(); // returns 0 if no current user

        if ( get_option('gwolle_gb-moderate-entries', 'true') == 'true' ) {
            if ( gwolle_gb_is_moderator($user_id) ) {
                $isChecked = true;
            }
        } else {

            $isChecked = true;

            // Check for abusive content (too long words). Set it to unchecked, so manual moderation is needed.
            $maxlength = 100;
            $words = explode( " ", $this->entry->get_content() );
            foreach ( $words as $word ) {
                if ( strlen($word) > $maxlength ) {
                    $isChecked = false;
                    break;
                }
            }

            $maxlength = 60;
            $words = explode( " ", $this->entry->get_author_name() );
            foreach ( $words as $word ) {
                if ( strlen($word) > $maxlength ) {
                    $isChecked = false;
                    break;
                }
            }
        }
        $this->entry->set_ischecked ( $isChecked );
    }

    private function validateName($name, $required = false) {
        $value = trim($name);
        
        if ( (!$value || $value == '') && $required ) {
            array_push($this->error, array(
                'field'   => 'name',
                'message' => 'Sie haben keinen Namen angegeben.'
            ));
            return false;
        } else {
        
            if ( !preg_match('/^[äöü ÄÖÜ \w \d]+$/ui', $value) ){
                $this->error[] = array(
                    'field'   => 'name',
                    'message' => 'Bitte geben sie einen gültigen Namen ein.'
                );
                return false;
            }
            $this->entry->set_author_name($value);
        }
        return true;
    }
    
    private function validateEmail ($email, $required = false) {
        $value = trim($email);
        
        // no value given but it is required
        if ( (!$value || $value == '') && $required) {
            $this->error[] = array(
                'field' => 'email',
                'message' => 'Sie haben keine Email angegeben.'
            );
            return false;
            
        } else if ( $value && $value != '' && !!is_email($email) === FALSE) {
            
            $this->error[] = array(
                'field'   => 'email',
                'message' => 'Email Addresse ist ungültig.'
            );
            return false;
            
        } else {
            /* @TODO need to change this will set empty email
             */
            if ( $value && $value != ''){
                $this->entry->set_author_email($value);
            }
        }
        return true;
    }
    
    /**
     * validate website
     * 
     * @param {string} $website
     * @param {boolean} required 
     */
    private function validateWebsite ($website, $required = false) {
        $value = trim($website);
        
        if ( (!$value || $value == '') && $required ) {
            $this->error[] = array(
                'field'   => 'website',
                'message' => 'Keine Webseite angegeben.'
            );
            return false;
        } else if ( $value && $value != '' && !!filter_var($value, FILTER_VALIDATE_URL) === FALSE) {
            
            $this->error[] = array(
                'field'   => 'website',
                'message' => 'Webseite konnte nicht gefunden werden.'
            );
            return false;
        } else {
            /* @TODO need to change this will set empty email
             */
            if ( $value && $value != ''){
                $this->entry->set_author_website($value);
            }
        }
        return true;
    }
    
    private function validateOrigin ($origin, $required = false) {
    }
    
    private function validateMessage ($message, $required = false) {
        $value = trim($message);
        gwolle_gb_maybe_encode_emoji( $value, 'content');
        
        if ( (!$value || $value == '' ) && $required ) {
            $this->error[] = array(
                'field'   => 'message',
                'message' => 'Sie haben keine Nachricht hinterlegt.'
            );
            return false;
        }
        $this->entry->set_content($value);
        return true;
    }
}
new Bowhunters_Guestbook();
